import math

def getKey(arr):
    return (arr[0], arr[1])

def pending(a,b):
    return (b[1]-a[1])/(b[0]-a[0])

def pLen(a,b):
    return math.sqrt(math.pow(b[0]-a[0],2)+math.pow(b[1]-a[1],2))

def cSize(arr):
    lenght = 0
    beg = 0
    size = len(arr)
    end = size-1
    line = [[]]*len(arr)
    for i in range(size):
        if (arr[0][1]>=arr[i][1]):
            line[beg] = arr[i]
            beg+=1
        else:
            line[end] = arr[i]
            end-=1
    for i in range(size-1):
        lenght+= pLen(line[i],line[i+1])
    return lenght



def minMaxY(l,r):
    n = {'l':{'min':0,'max':0},'r':{'min':0,'max':0}}
    for i in range(len(l)):
        if (l[i][1] > l[n['l']['max']][1]):
            n['l']['max']=i
        if (l[i][1] < l[n['l']['min']][1]):
            n['l']['min']=i
    for i in range(len(r)):
        if (r[i][1] > r[n['r']['max']][1]):
            n['r']['max']=i
        if (r[i][1] < r[n['r']['min']][1]):
            n['r']['min']=i
    return n


def merge(l,r):
    n = minMaxY(l,r)
    outP = {"l":0,"r":0}
    ##Finding outside points
    runp = None
    rdnp = None
    lunp = None
    ldnp = None
    if (l[n['l']['max']][1] >r[n['r']['max']][1]): ##Left is upper than right
        lp = l[n['l']['max']]
        rp = r[n['r']['max']]
        i = n['l']['max'] + 1
        while(i<len(l)):
            if (l[i][1] > rp[1]):
                if (pending(lp,l[i])>pending(lp,rp)):
                    lunp = i
                    i+=1
                # else:
                #     del l[i]
            i+=1
    if (l[n['l']['min']][1] <r[n['r']['min']][1]): ##Left is lower than right
        lp = l[n['l']['min']]
        rp = r[n['r']['min']]
        i = n['l']['min']+ 1
        while(i<len(l)):
            if (l[i][1] < rp[1]):
                if (pending(lp,l[i])<pending(lp,rp)):
                    ldnp = i
                    i+=1
                # else:
                #     del l[i]
            i+=1
    if (l[n['l']['max']][1] <r[n['r']['max']][1]): ##Right is upper than left
        lp = l[n['l']['max']]
        rp = r[n['r']['max']]
        i = n['r']['max'] - 1
        while(i>=0):
            if (r[i][1] > lp[1]):
                if (pending(rp,r[i])>pending(rp,lp)):
                    runp = i
                # else:
                #     del r[i]
            i-=1
    if (l[n['l']['min']][1] >r[n['r']['min']][1]): ##Right is lower than left
        lp = l[n['l']['min']]
        rp = r[n['r']['min']]
        i = n['r']['min'] - 1
        while(i>=0):
            if (r[i][1] < lp[1]):
                if (pending(rp,r[i])<pending(lp,rp)):
                    rdnp = i
                # else:
                #     del r[i]
            i-=1
    il = min(n['l']['min'],n['l']['max'])
    ir = min(n['r']['min'],n['r']['max'])
    for i in range(il,len(l)):
        if (i!=n['l']['min'] and i!=n['l']['max']):
            if ((lunp==None or lunp!=i) and (ldnp==None or ldnp!=i)):
                l[i] = None
    for i in range(ir,-1,-1):
        if (i!=n['r']['min'] and i!=n['r']['max']):
            if ((runp==None or runp!=i) and (rdnp==None or rdnp!=i)):
                r[i] = None
    #merge 2 CH
    total = []
    for p in l:
        if (p!=None):
            total.append(p)
    for p in r:
        if (p!=None):
            total.append(p)
    return total


def convexHull(array):
    if (len(array)<=3):
        return array
    size = len(array)
    mid = (int) (size/2)
    leftCH = convexHull(array[0:mid])
    rightCH = convexHull(array[mid:size])
    totalCH = merge(leftCH,rightCH)
    return totalCH
