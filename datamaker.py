# Descarga Numpy: http://www.scipy.org/install.html (Recomendaria Anaconda aunque te descarga como 100 librerias que no creo que uses)
import numpy as np

def random_points(amount):
	return np.random.rand(amount, 2)

def normal_points(amount):
	mean = [1, 2]
	conv = [[1, 0], [0, 1]]
	return np.random.multivariate_normal(mean, conv, amount)

if __name__ == '__main__':
	from sys import argv, exit
	if len(argv) != 4:
		exit()
	size = argv[2]
	direc = argv[3]
	if argv[1] == '-r':
		a = random_points(int(size))
		np.savetxt(direc + size + '.txt', a)
	elif argv[1] == '-n':
		a = normal_points(int(size))
		np.savetxt(direc + size + '.txt', a)
	else:
		exit()
