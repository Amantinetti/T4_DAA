from convexHull import *

def DivideAndConquerMin(array):
    solve = sorted(array, key=getKey)
    solve = convexHull(solve)
    return cSize(solve)
    ##return solve


if __name__ == '__main__':
    from timeit import Timer
    from sys import argv, exit
    import numpy as np
    if (len(argv) < 2):
        exit()
    routes = argv[1:len(argv)]
    for route in routes:
        print('Test -> ' + route )
        oFile = np.loadtxt(route)
        a = [(oFile[i][0], oFile[i][1]) for i in range(len(oFile))]
        samples = 10
        t = Timer("DivideAndConquerMin(a)", "from __main__ import DivideAndConquerMin, a")
        took = t.timeit(samples)/samples
        print("DivideAndConquerMin for {} integers took {:8f} secs".format(len(a), took))
